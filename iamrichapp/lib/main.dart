import 'package:flutter/material.dart';

import 'dart:ui';
import 'package:flutter_blurhash/flutter_blurhash.dart';

//..
void main() {
  var blurImage = 'https://i.ibb.co/YRNvDX0/246-2r8yd-F9nq-Qc-O2owt.jpg';
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          title: Text('I Am Rich'),
          backgroundColor: Colors.blueGrey[900],
        ),
        body: Center(
          child: BlurHash(
            hash: 'LCO:5}9Yu5.91lNg-p?^5RJRxDrW',
            imageFit: BoxFit.cover,
            image: blurImage,
          ),
        ),
      ),
    ),
  );
}
